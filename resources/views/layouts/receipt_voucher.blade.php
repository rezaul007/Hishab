@extends('layouts.dashboard')
@section('content')
<?php 
   $user_id=Session::get('user_id'); 
   use App\ac_ledgermodel;
   use App\clientmodel;


   $model = new ac_ledgermodel();
   $head_name= $model->view($user_id);

   $model2 = new clientmodel();
   $supplier = $model2->view($user_id);
?>

    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add receipt voucher</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{!! url('store_rcv_voucher') !!}" method="POST">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                  <input type="text" name="date" id="date" class="form-control" id="exampleInputEmail1" placeholder="Enter date">
                </div>
                <label for="exampleInputEmail1">Description</label>
                  <input type="text" name="description" id="description" class="form-control" id="exampleInputEmail1" placeholder="Enter description">
                </div>
                <div class="form-group">
                  <label>account head</label>
                    <select class="form-control" name="ac_head" id="ac_head">
                    <option> </option>
                     <?php 
                  foreach ($head_name as $v) { 
                  ?>
                 <?php echo"<option value='$v->id ' > $v->ledger_name </option>" ?>

                 <?php } ?>

                    
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Amount</label>
                  <input type="text" name="amount" id="amount" class="form-control" id="exampleInputEmail1" placeholder="Enter amount in taka">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Receive from</label>
                  <select class="form-control" name="rcv_from" id="rcv_from">
                    <option> </option>
                    <?php
                    foreach ($supplier as $vl) {
                      ?>
                 <?php echo"<option value='$vl->name ' > $vl->name </option>" ?>

                 <?php } ?>


                  </select>
                </div>
                <?php foreach ($supplier as $vl){ ?>
                <input type="hidden" name="address" value="<?php echo $vl->password ; ?>">
              <?php } ?>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>

            <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
@stop