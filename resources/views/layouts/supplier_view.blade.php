@extends('layouts.dashboard')
@section('content')
<div class="box-body table-responsive no-padding">
<p><?php if(!empty(Session::get('msg'))) { echo Session::get('msg'); } ?> </p>
<p>supplier view </p>
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>name</th>
                  <th>Org_name</th>
                  <th>Category</th>
                </tr>
                <?php
                foreach ($supplier as $value) {
                ?>
                <tr>
                  <?php echo "<td> $value->id; </td>" ?>
                  <?php echo "<td> $value->name; </td>" ?>
                  <?php echo "<td> $value->org_name; </td>" ?>
                  <?php echo "<td> $value->category; </td>" ?>
                  <td><a href="{!! url('supplier_edit',array('id'=>$value->id)) !!} "><span class="label label-success">edit</span></a></td>
                  <td><a href="{!! url('supplier_delete',array('id'=>$value->id)) !!}" onclick="return getConfirmation();" ><span class="label label-success">delete</span></a></td>
                </tr>
                <?php } ?> 
              </table>
            </div>


            <script type="text/javascript">
         <!--
            function getConfirmation(){
               var retVal = confirm("Do you want to delete ?");
               if( retVal == false ){
                  
                  return false;
               }
               else{
                  
                  return true;
               }
            }
         //-->
      </script>
      
            @stop 