@extends('layouts.dashboard')
@section('content')

    <?php
    use App\suppliermodel;
    use App\clientmodel;
    $user_id=Session::get('user_id');

    $model = new suppliermodel();
    $supplier = $model->view($user_id);


    $model2 = new clientmodel();
    $client=$model2->view($user_id);
    $selected=$v;
    $selectedd=$vl;
    $selecteddd=$vll;


    ?>


    <section class="content">
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                        <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">INVOICE</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{!! url('view_client') !!}" method="POST">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <div class="form-group">
                                <label> Select Client</label>
                                <select class="form-control" name="client" id="client" onchange="valueCheck1()">
                                    <option><?php if($selectedd != null){echo $selectedd;}  ?> </option>
                                    <?php
                                    foreach ($client as $vl) {
                                        if( $selected == $vl->name){?>
                                    <?php echo"<option  value='$vl->name ' selected='selected' > $vl->name </option>" ?>
                                       <?php } else{ ?>
                                        ?>
                 <?php echo"<option value='$vl->name ' > $vl->name </option>" ?>

                 <?php } } ?>
                                <option> </option>
                                </select>

                                <br>
                                <label> Select Supplier</label>
                                <select class="form-control" name="supplier" id="supplier" onchange="valueCheck2()" >
                                    <option><?php if($selecteddd != null){echo $selecteddd;}  ?> </option>
                                    <?php
                                    foreach ($supplier as $v) {
                                        ?>
                 <?php echo"<option value='$v->name ' > $v->name </option>" ?>

                 <?php } ?>
                                <option> </option>
                                </select>
                                <br>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                    </div>
                    <!-- /.box-body -->


                    </form>



                <br/>


                <div class="box-body table-responsive no-padding" id="invoice_table">
                    <?php if( $invoice != null){ ?>
                      <?php  $amount=00.00; ?>

                        <a href="{!! url('pdfview',array('selected'=>$selected,'amount'=>$amount)) !!} "><button type="submit" class="btn btn-primary">Download PDF</button></a>


                    <?php
                    foreach ($invoice as $value) {


                    ?>

                    <?php } ?>
                    <table class="table table-hover">
                        <tr>
                            <th>Date</th>
                            <th>Client Address</th>
                            <th>Item Description</th>
                            <th>Amount</th>
                        </tr>
                        <?php
                        foreach ($invoice as $value) {

                        ?>
                        <tr>
                            <?php echo "<td> $value->date; </td>" ?>
                            <?php echo "<td> $value->clisub_address; </td>" ?>
                            <?php echo "<td> $value->description; </td>" ?>
                            <?php echo "<td> $value->amount; </td>" ?>
                            <?php $amount +=$value->amount; ?>

                        </tr>
                        <?php  } ;?>
                        <tr>
                            <td colspan="3">Total</td>
                            <td><?php echo $amount ?></td>
                        </tr>

                    </table>
                    <?php } ?>

                </div>




@stop
                    <script>
                        function valueCheck1() {
                            var x = document.getElementById("client").value;
                            var y = document.getElementById("supplier").value;
                            if(x!=""){
                                document.getElementById("supplier").value ="";
                            }

                        }
                        function valueCheck2() {
                            var x = document.getElementById("client").value;
                            var y = document.getElementById("supplier").value;
                            if(y!=""){
                                document.getElementById("client").value = "";
                            }

                        }
                    </script>