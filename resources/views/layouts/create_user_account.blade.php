<!DOCTYPE html>
<html>
<head>
     <link rel="stylesheet" href="{{asset('login/login.css') }}">
</head>
<body>
  <div class="login-page">
  <div class="form">
    <form class="register-form">
      <input type="text" placeholder="name"/>
      <input type="password" placeholder="password"/>
      <input type="text" placeholder="email address"/>
      <button>create</button>
      <p class="message">Already registered? <a href="#">Sign In</a></p>
    </form>
    <form class="login-form" action="{!! url('dashboard_store') !!}" method="POST">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="text" name="org_name" placeholder="Organization name"/>
      <input type="text" name="email" placeholder="email address"/>
      <select name="org_type">
        <option>select an organization type</option>
        <option>IT</option>
        <option>Finance</option>
        <option>Marketing</option>
        <option>Cloth</option>
      </select>
      <input type="number" name="contact_no" placeholder="contact no."/>
      <input type="text" name="org_address" placeholder="organization address"/>
      <input type="password" name="password" placeholder="password"/>
      <button>Save</button>
    </form>
  </div>
</div>
<script type="text/javascript">
$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});
</script>
</body>
</html>



