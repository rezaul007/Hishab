<!DOCTYPE html>
<html>
<head>
     <link rel="stylesheet" href="{{asset('login/login.css') }}">
</head>
<body>
  <div class="login-page">
  <div class="form">
    <form class="register-form">
      <input type="text" name="name" placeholder="name"/>
      <input type="password" name="password" placeholder="password"/>
      <input type="text" placeholder="email address"/>
      <button>create</button>
      <p class="message">Already registered? <a href="#">Sign In</a></p>
    </form>
    <form class="login-form" action="{!! url('user_login') !!}" method="POST">
     <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="text" name="name" placeholder="username"/>
      <input type="password" name="password"  placeholder="password"/>
      <button>login</button>
        <p><?php if(!empty(Session::get('log_error'))) {  echo  Session::get('log_error'); } ?> </p>
      <p class="message">Not registered? <a href="{!!url('create_user_account')!!}">Create an account</a></p>
    </form>
  </div>
</div>
<script type="text/javascript">
$('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});
</script>
</body>
</html>


