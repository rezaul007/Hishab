@extends('layouts.dashboard')
@section('content')

    <?php
    //echo '<pre>';
    //var_dump($sn);die;
    /*echo '<pre>';
    var_dump($voucher);die;*/
    /*$user_id=\Illuminate\Support\Facades\Session::get('user_id');
    use App\ledger_group_model;
    $model= new ledger_group_model();
    $table_vale=$model->raw_count($user_id);
    foreach ($table_vale as $count) {

      $val= $count->count;
    }*/

    ?>
    <!-- <a href="{{ url('downloadExcel/xlsx') }}"><button class="btn btn-success btn-lg">Download Excel xlsx</button></a> -->
    <div class="box-body table-responsive no-padding">
        <p><?php if(!empty(\Illuminate\Support\Facades\Session::get('msg'))) { echo    \Illuminate\Support\Facades\Session::get('msg'); } ?> </p>
        <p> Trail Balance statement</p>
        <table class="table table-hover">
            <tr>
                <th>S/N</th>
                <th>Head of the account</th>
                <th>Debit amount</th>
                <th>Credit amount</th>
            </tr>
            <?php
            $debit=0;
            $credit=0;
            $previous_group_name='';
            $previous_debit=0;
            $previous_credit=0;
            foreach ($voucher as $value){
            $type=$value->transaction_type;
            $group=$value->group_name;

            ?>
            <tr colspan="4">

                <td > <?php
                    if ($previous_group_name!=$value->group_name){
                        echo $value->group_name;

                    } ?>  </td>
                <?php
                echo "<td>  $value->ledger_name </td>";
                ?>
                <?php
                if($type == "Debit") {
                    echo "<td> $value->tm  </td>" ;
                    echo "<td> 00.00 </td>";
                    if ($previous_group_name==$value->group_name){
                        $debit +=  $value->tm;
                        $previous_debit=$debit;
                        $credit=00.00;
                    }
                    else{
                        $debit=$value->tm;
                        $credit=00.00;
                    }
                }
                elseif($type == "Credit"){
                    echo  "<td> 00.00 </td>";
                    echo "<td>  $value->tm   </td>" ;
                    if ($previous_group_name==$value->group_name){
                        $credit +=$value->tm;
                        $previous_credit=$credit;
                        $debit=00.00;
                    }
                    else{
                        $credit=$value->tm;
                        $debit=00.00;
                    }
                }
                ?>
            </tr>
            <?php if ($previous_credit!=0 ||$previous_debit!=0){?>
            <tr>
                <td colspan="2"> Total</td>
                <td> <?php echo $debit ?></td>
                <td><?php echo $credit ?></td>
            </tr>
            <?php }
            $previous_group_name=$value->group_name;

            } ?>
        </table>
    </div>


@stop