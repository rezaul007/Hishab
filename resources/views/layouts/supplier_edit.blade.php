@extends('layouts.dashboard')
@section('content')
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">supplier add form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{!! url('supplier_update') !!}" method="POST">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="id" value="<?php  echo $id; ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">name</label>
                  <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $name; ?>">
                </div>
                <label for="exampleInputEmail1">organization</label>
                  <input type="text" name="org_name" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php echo $org_name; ?>">
                </div>
                <div class="form-group">
                  <label>category</label>
                  <select class="form-control" name="category">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
                
  
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
@stop
