@extends('layouts.dashboard')
@section('content')
<?php 
use App\ledger_group_model;
$model= new ledger_group_model();
$user_id=Session::get('user_id'); 
$lg=$model->view($user_id);

?>

    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add A/C Ledger</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{!! url('ac_ledger_store') !!}" method="POST">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Ledger name</label>
                  <input type="text" name="ledger_name" id="ledger_name" class="form-control" id="exampleInputEmail1" placeholder="Enter date">
                </div>
                </div>
                 
                <div class="form-group">
                
                <label>Select ledger group</label>
               
                 
                 <select class="form-control" name="ledger_group">
                 <option> </option>
                  <?php 
                  foreach ($lg as $v) { 
                  ?>
                 <?php echo"<option value='$v->id' > $v->group_name </option>" ?>

                 <?php } ?>

                  </select>

              
                </div>
                

                 <div class="form-group">
                  <label>tranaction type</label>
                  <select class="form-control" name="tranaction_type">
                    <option> </option>
                    <option>Debit</option>
                    <option>Credit</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Date</label>
                  <input type="text" name="date" id="date" class="form-control" id="exampleInputEmail1" placeholder="Enter date">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          
            

            <div class="box-body table-responsive no-padding">
<p><?php if(!empty(Session::get('msg'))) { echo  Session::get('msg'); } ?> </p>
<p>account ledger view </p>
              <table class="table table-hover">
                <tr>
                  <th>Ledger name</th>
                  <th>Ledger group</th>
                  <th>Transaction type</th>
                  <th>Date</th>
                </tr>
                <?php
                foreach ($ac_ledger as $value) {
                  foreach ($lg as $vl) {
                    if($value->ledger_group_id == $vl->id) {
                  
                ?>
                <tr>
                  <?php echo "<td> $value->ledger_name; </td>" ?>
                  <?php echo "<td> $vl->group_name; </td>" ?>
                  <?php echo "<td> $value->transaction_type; </td>" ?>
                  <?php echo "<td> $value->date; </td>" ?>
                  <td><a href="{!! url('ac_ledger_delete',array('id'=>$value->id)) !!}" onclick="return getConfirmation();"><span class="label label-success">delete</span></a></td>
                </tr>
                <?php } } } ?> 
              </table>
            </div>

          <script type="text/javascript">
             $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
  <script type="text/javascript">
         <!--
            function getConfirmation(){
               var retVal = confirm("Do you want to delete ?");
               if( retVal == false ){
                  
                  return false;
               }
               else{
                  
                  return true;
               }
            }
         //-->
      </script>
      
@stop