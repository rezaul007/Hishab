@extends('layouts.dashboard')
@section('content')
    <div class="box-body table-responsive no-padding">
        <div class="box-footer">
            <form action="{!! url('profile_edit') !!}">
            <button type="submit" class="btn btn-primary">Edit profile</button>
            </form>
            <form action="{!! url('pass_from') !!}">
            <button type="submit" class="btn btn-primary">Reset password</button>
            </form>
            <form action="{!! url('img_from') !!}">
                <button type="submit" class="btn btn-primary">Reset Company Logo</button>
            </form>
        </div>
        <p><?php if(!empty(Session::get('msg'))) {  echo  Session::get('msg'); } ?> </p>
        <p>Organization Proifile </p>
        <table class="table table-hover">
            <tr>
                <th>Organization name</th>
                <th>Email</th>
                <th>organization type</th>
                <th>contact no</th>
                <th>Address</th>

            </tr>
            <?php
            foreach ($voucher as $value) {
            ?>
            <tr>
                <?php echo "<td> $value->org_name; </td>" ?>
                <?php echo "<td> $value->email; </td>" ?>
                <?php echo "<td> $value->org_type; </td>" ?>
                <?php echo "<td> $value->contact_no; </td>" ?>
                <?php echo "<td> $value->org_address; </td>" ?>

            </tr>
            <?php } ?>
        </table>
    </div>

    <script type="text/javascript">
        <!--
        function getConfirmation(){
            var retVal = confirm("Do you want to delete ?");
            if( retVal == false ){

                return false;
            }
            else{

                return true;
            }
        }
        //-->
    </script>

@stop