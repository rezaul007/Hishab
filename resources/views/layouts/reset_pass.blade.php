@extends('layouts.dashboard')
@section('content')

    <section class="content">
        <div class="row">
            <p><?php if(!empty(\Illuminate\Support\Facades\Session::get('msg'))) { echo    \Illuminate\Support\Facades\Session::get('msg'); } ?> </p>
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reset password</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{!! url('pass_update') !!}" method="POST" onsubmit="return confirmpass()">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter current passwoed</label>
                                <input type="password" name="curr_pass" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Enter New Password</label>
                            <input type="password" name="new_password" id="new_password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Confirm Password</label>
                                <input type="password" name="re_new_password" id="re_new_password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
    <script>
        function confirmpass() {
            var pass1 = ("#new_password").val();
            var pass2 = ("#re_new_password").val();
            if(pass1 != pass2){
                alert("password not match");
                return false;
            }
            else{
                return true;
            }
        }
    </script>
@stop