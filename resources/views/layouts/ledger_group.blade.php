@extends('layouts.dashboard')
@section('content')

    <section class="content">
      <div class="row">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
              @endif

                      <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add ledger group</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{!! url('add_ledger_group') !!}" method="POST">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Group name</label>
                  <input type="text" name="group_name" id="date" class="form-control" id="exampleInputEmail1" placeholder="Enter date">
                </div>
                </div>
                <div class="form-group">
                  <label> Seect Class</label>
                  <select class="form-control" name="class">
                    <option> </option>
                    <option>Asset</option>
                    <option>Income</option>
                    <option>Expense</option>
                    <option>Liabilities</option>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>

            

            <div class="box-body table-responsive no-padding">
<p><?php if(!empty(Session::get('msg'))) { echo Session::get('msg');Session::forget('msg'); } ?> </p>
<p>voucher view </p>
              <table class="table table-hover">
                <tr>
                  <th>group name</th>
                  <th>class</th>
                </tr>
                <?php
                foreach ($ledger_group as $value) {
                ?>
                <tr>
                  <?php echo "<td> $value->group_name; </td>" ?>
                  <?php echo "<td> $value->class; </td>" ?>
                  <td><a href="{!! url('delete_ledger_group',array('id'=>$value->id)) !!}"><span class="label label-success" onclick="return getConfirmation();" >delete</span></a></td>
                </tr>
                <?php } ;?> 
              </table>
            </div>

  <script type="text/javascript">
         <!--
            function getConfirmation(){
               var retVal = confirm("Do you want to delete ?");
               if( retVal == false ){
                  
                  return false;
               }
               else{
                  
                  return true;
               }
            }
         //-->
      </script>
       
@stop