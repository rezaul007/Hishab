
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
       
      </div>


      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->



      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Supplier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{!! url('supplier_add') !!}"><i class="fa fa-circle-o"></i> Add supplier</a></li>
            <li><a href="{!! url('supplier_view') !!}"><i class="fa fa-circle-o"></i>View supplier</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Client</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{!! url('add_client') !!}"><i class="fa fa-circle-o"></i> Add client</a></li>
            <li><a href="{!! url('client_view') !!}"><i class="fa fa-circle-o"></i>View client</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Voucher</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{!! url('add_rcv_voucher') !!}"><i class="fa fa-circle-o"></i> Receipt voucher</a></li>
            <li><a href="{!! url('add_pay_voucher') !!}"><i class="fa fa-circle-o"></i>Payment voucher</a></li>
            <li><a href="{!! url('view_voucher') !!}"><i class="fa fa-circle-o"></i>View voucher</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Ledger</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{!! url('view_ledger_group') !!}"><i class="fa fa-circle-o"></i> Ledger group</a></li>
            <li><a href="{!! url('ac_ledger_view') !!}"><i class="fa fa-circle-o"></i>A/C ledger</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{!! url('trial_balence') !!}"><i class="fa fa-circle-o"></i>Trial balence</a></li>
            <li><a href="{!! url('view_invoice') !!}"><i class="fa fa-circle-o"></i>Invoice</a></li>

          </ul>
        </li>
        
    <!-- /.sidebar -->
  