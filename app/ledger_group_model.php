<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class ledger_group_model extends Model
{
     public function add_ledger_group($group_name,$class,$user_id){
    	$user=DB::table('ledger_group')
    	->insert( ['group_name'=>$group_name,'class'=>$class,'user_id'=>$user_id] );
 
    	return true;

    }
    public function view($user_id){
        $user=DB::table('ledger_group')
        ->select(DB::raw('*'))
        ->where('user_id','=',$user_id)
        ->get();
        return $user;
    }
    public function lg_delete($id){
        $user=DB::table('ledger_group')
        ->where('id','=',$id)
    	->delete();
    	return true;
    }
    public function raw_count($user_id){
        $user=DB::table('ledger_group')
        ->select(DB::raw('count(*) as count'))
        ->where('user_id','=',$user_id)
        ->get();
        return $user;
    }
}
