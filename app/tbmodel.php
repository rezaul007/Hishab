<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class tbmodel extends Model
{
    public function sn(){
        $query=DB::select("SELECT c.ledger_name,d.group_name FROM (SELECT voucher.accounting_ledger FROM voucher)b LEFT JOIN (SELECT account_ledger.ledger_name,account_ledger.id,account_ledger.ledger_group_id FROM account_ledger )c ON b.accounting_ledger=c.id LEFT JOIN (SELECT ledger_group.id,ledger_group.group_name FROM ledger_group )d ON d.id=c.ledger_group_id GROUP BY d.group_name");
        return $query;
    }

    public function view($user_id){
    	 $query=DB::select("SELECT c.group_name,b.ledger_name,b.transaction_type,d.tm FROM (SELECT * FROM `account_ledger` ORDER BY `ledger_group_id`)b 
    	 	LEFT JOIN (SELECT ledger_group.group_name ,ledger_group.id FROM ledger_group )c ON b.ledger_group_id=c.id
            LEFT JOIN (SELECT `accounting_ledger`, SUM(`amount`) AS tm FROM voucher WHERE voucher.user_id=$user_id GROUP BY `accounting_ledger` )d ON b.id=d.accounting_ledger ORDER BY c.group_name ASC ");
        return $query;
        
    }
}