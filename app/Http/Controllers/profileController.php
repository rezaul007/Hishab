<?php

namespace App\Http\Controllers;

use App\profileModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class profileController extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function edit(Request $request){
        $user_id=$request->session()->get('user_id');
        $model= new profileModel();
        $client=$model->view($user_id);
        foreach ($client as $v){
            $org_name=$v->org_name;
            $email=$v->email;
            $org_type=$v->org_type;
            $contact_no=$v->contact_no;
            $org_address=$v->org_address;
            $id=$v->user_id;
        }
        return view('layouts/edit_profilr')->with(compact('org_name','org_type','org_address','email','contact_no','id'));
    }
    public function update_profile(Request $request){

        $id=$_POST['id'];
        $org_name=$_POST['org_name'];
        $email = $_POST['email'];
        $contact_no = $_POST['contact_no'];
        $address = $_POST['address'];
        $org_type = $_POST['org_type'];

        $model= new profileModel();
        $client=$model->update_profile($org_name,$email,$contact_no,$address,$org_type,$id);
        if($client != null){
            $request->session()->put('msg','update successfull');
            $msg=$request->session()->get('msg');
            return redirect('profile_view')->with(compact('msg'));

        }
        else{
            $request->session()->put('msg','update successfull');
            $msg=$request->session()->get('msg');
            return redirect('profile_view')->with(compact('msg'));
        }
    }
    public function view(Request $request){
        $user_id=$request->session()->get('user_id');
        $model= new profileModel();
        $voucher=$model->view2($user_id);
        return view('layouts/profile_view')->with(compact('voucher'));
    }
    public function view_pass_from(){
        return view("layouts/reset_pass");
    }

    public function resetPass(Request $request){
        $this->validate($request, [
            'new_password' => 'required',
            'curr_pass' => 'required',
            're_new_password' => 'required',
        ]);
        $user_id=$request->session()->get('user_id');
        $model= new profileModel();
        $new_pass=md5($_POST['new_password']);

            $value=$model->confpass($user_id);
        foreach ($value as $v){
            $old_pass=$v->password;
        }
        $old_pass2=md5($_POST['curr_pass']) ;
        if($old_pass==$old_pass2){
            $pass=$model->new_pass($new_pass);

            if ($pass !=null){
                $request->session()->put('msg','reset password successfully');
                $msg=$request->session()->get('msg');
                return redirect('profile_view')->with(compact('msg'));
            }
            else{

                $request->session()->put('msg','password reset failed!!!');
                $msg=$request->session()->get('msg');
                return redirect('profile_view')->with(compact('msg'));
            }


        }
        else{
            $request->session()->put('msg','your currenr password is not correct!!!');
            $msg=$request->session()->get('msg');
            return redirect('profile_view')->with(compact('msg'));
        }


    }
    public function img_from(){
        return view('layouts/image');
    }
    public function img_upload(Request $request){
        $this->validate($request, [
            'image' => 'required',
        ]);
        $user_id=$request->session()->get('user_id');
        $image = $request->input('image');

        if($_FILES['image']['name']!=null) {
            $filename = $_FILES['image']['name'];
            $info = pathinfo(storage_path() . $filename);
            $ext = $info['extension'];
            $file_name = $user_id . '.' . $ext;
            $path = public_path("images");
            $uploadSuccess = Input::file('image')->move($path, $file_name);
            if ($uploadSuccess != null) {
                $model= new profileModel();
                $img= $model->img_up($file_name,$user_id);
                $request->session()->put('msg', 'Image upload successful..');
                $msg=$request->session()->get('msg');
                return redirect('profile_view')->with($msg);
            }
            else{
                $request->session()->put('msg', 'Image upload unsuccessful..');
                $msg=$request->session()->get('msg');
                return redirect('profile_view')->with($msg);
            }

        }
    }

    public function update(Request $request)
    {
        //echo '<pre>';


    }

}
