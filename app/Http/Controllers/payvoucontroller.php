<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\rcvvouchermodel;
use Illuminate\Support\Facades\Redirect;

class payvoucontroller extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(){
    	return view('layouts/pay_vou');
    }
    public function store(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'description' => 'required',
            'ac_head' => 'required',
            'amount' => 'required',
            'pay_to' => 'required',
            
        ]);
      $user_id=$request->session()->get('user_id');
      $date=$_POST['date'];
      $description=$_POST['description'];
      $ac_head=$_POST['ac_head'];
      $amount='-'.$_POST['amount'];
      $pay_to=$_POST['pay_to'];
      $voucher_type="payment";
        $address = $_POST['address'];
      $model=new rcvvouchermodel();
  $voucher=$model->add_voucher($date,$description,$ac_head,$amount,$pay_to,$voucher_type,$user_id,$address);
      if($voucher != null){
        $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('view_voucher')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','store failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('view_voucher')->with(compact('msg'));
      }

    }
    public function view(Request $request){
    	$user_id=$request->session()->get('user_id');
    	$model=new rcvvouchermodel();
    	$voucher=$model->view($user_id);
    	return view('layouts/view_voucher')->with(compact('voucher'));
    }
    public function curramount(){
      $user_id=Session::get('user_id');
      $val=$_POST['val'];
      $model=new rcvvouchermodel();
      $amount= $model->curr_amount($val, $user_id);
     if(!empty($amount)){
       foreach ($amount as $vl) {
        $v=$vl->amount;
        //echo '<input type="number" name="curr_amount" id="curr_amount" class="form-control" value="'.$vl->amount.'" >';
       }
      echo json_encode($v);
     }
    }
}
