<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\suppliermodel;
use Illuminate\Support\Facades\Redirect;

class suppliercontroller extends Controller
{

    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request){
      $user_id=$request->session()->get('user_id');
    	$model= new suppliermodel();
    	$supplier=$model->view($user_id);
    	/*foreach ($supplier as $value) {
    		$name=$value->name;
    		$id=$value->id;
    		$org_name=$value->org_name; 
    		$category=$value->category;
    	}*/
    	return view('layouts/supplier_view')->with(compact('supplier'));
    }
    public function create(){
    	return view('layouts/supplier_add');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'org_name' => 'required',
            'password' => 'required',
            'category' => 'required',

        ]);
    	$user_id=$request->session()->get('user_id');
    	$name = $_POST['name'];
    	$org_name = $_POST['org_name'];
    	$category = $_POST['category'];
    	$password = md5($_POST['password']);

    	$model= new suppliermodel();
    	$supplier = $model->add_supplier($user_id,$name,$org_name,$category,$password);
    	if($supplier == true){
        $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','store failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
      }

    }
    public function edit(Request $request, $id){
       $model= new suppliermodel();
       $supplier=$model->edit($id);
       foreach ($supplier as $value) {
       	    $id=$value->id;
       	    $name=$value->name;
       	    $org_name=$value->org_name;
       	    $category= $value->category;
       	    $password= $value->password;
       }
       return view('layouts/supplier_edit')->with(compact('id','name','org_name','category','password'));


    }
    public function update_supplier(Request $request){

    	$id=$_POST['id'];
        $name = $_POST['name'];
    	$org_name = $_POST['org_name'];
    	$category = $_POST['category'];
    	$model= new suppliermodel();
    	$supplier=$model->update_supplier($id,$name,$org_name,$category);
      if($supplier == true){
        $request->session()->put('msg','update successfull');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
      }
      else{
          $request->session()->put('msg','update failed!!!!');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
      }
    }
    public function delete_supplir(Request $request,$id){
    	$model= new suppliermodel();
    	$delete=$model->supplier_delete($id);
    	if($delete != null){
        $request->session()->put('msg','delete successfull');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','delete failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('supplier_view')->with(compact('msg'));
      }
    }

}
