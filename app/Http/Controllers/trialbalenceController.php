<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\tbmodel;
use Illuminate\Support\Facades\Redirect;



class trialbalenceController extends Controller
{
	public function __construct(){

		$user_id=\Illuminate\Support\Facades\Session::get('user_id');
		if($user_id== null){
			Redirect::to('/')->send();
		}
	}
     public function index(Request $request){
    	$user_id=$request->session()->get('user_id');
    	$model=new tbmodel();
    	$sn=$model->sn();
    	$voucher=$model->view($user_id);


         return view('layouts/trialbalance')->with(compact('voucher','sn'));
    }
}
