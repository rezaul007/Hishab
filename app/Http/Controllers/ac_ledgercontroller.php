<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ac_ledgermodel;
use Illuminate\Support\Facades\Redirect;

class ac_ledgercontroller extends Controller
{
    public function __construct(){
        
        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request){
    	$user_id=$request->session()->get('user_id');
    	$model=new ac_ledgermodel();
    	$ac_ledger=$model->view($user_id);
        //var_dump($ac_ledger);
        //die;
    	return view('layouts/account_ledger')->with(compact('ac_ledger'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'ledger_name' => 'required',
            'ledger_group' => 'required',
            'tranaction_type' => 'required',
            'date' => 'required',
        ]);
      $user_id=$request->session()->get('user_id');	
      $ledger_name=$_POST['ledger_name'];
      $ledger_group=$_POST['ledger_group'];
      $tranaction_type=$_POST['tranaction_type'];
      $date=$_POST['date'];

      $model=new ac_ledgermodel();
      $ledger_group=$model->add_ac_ledger($ledger_name,$ledger_group,$tranaction_type,$date,$user_id);
      if($ledger_group != null){
        $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('ac_ledger_view')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','store failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('ac_ledger_view')->with(compact('msg'));
      }

    }
    public function ac_ledger_delete(Request $request,$id){
    	$model=new ac_ledgermodel();
    	$delete=$model->ac_lg_delete($id);
    	if($delete != null){
        $request->session()->put('msg','delete successfull');
        $msg=$request->session()->get('msg');
        return redirect('ac_ledger_view')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','delete failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('ac_ledger_view')->with(compact('msg'));
      }
    }
}
