<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ledger_group_model;
use Illuminate\Support\Facades\Redirect;

class ledger_group_controller extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request){
    	$user_id=$request->session()->get('user_id');
    	$model=new ledger_group_model();
    	$ledger_group=$model->view($user_id);
    	return view('layouts/ledger_group')->with(compact('ledger_group'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'group_name' => 'required',
            'class' => 'required',
        ]);
      $user_id=$request->session()->get('user_id');	
      $group_name=$_POST['group_name'];
      $class=$_POST['class'];

      $model=new ledger_group_model();
      $ledger_group=$model->add_ledger_group($group_name,$class,$user_id);
      if($ledger_group == true){
        $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('view_ledger_group')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','store failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('view_ledger_group')->with(compact('msg'));
      }

    }
    public function delete_ledger_group(Request $request,$id){
    	$model=new ledger_group_model();
    	$delete=$model->lg_delete($id);
    	if($delete == true){
        $request->session()->put('msg','delete successfull');
        $msg=$request->session()->get('msg');
        return redirect('view_ledger_group')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','delete failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('view_ledger_group')->with(compact('msg'));
      }
    }
}
