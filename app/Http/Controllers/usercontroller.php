<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\authmodel;

class usercontroller extends Controller
{
    public function index(){
    	return view('layouts/create_user_account');
    }
    public function login(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',

        ]);
        $name=$_POST['name'];
        $password= md5($_POST['password']);

        $authmodel = new authmodel();

        $login=$authmodel->login($name,$password);

        if(count($login) > 0){
            foreach ($login as $value) {
              	$user_id=$value->user_id;
              	$name=$value->org_name;
              }
               	$request->session()->put('user_id',$user_id);
              	$request->session()->put('name',$name);
              	$sid=$request->session()->get('user_id');
              	$sname=$request->session()->get('name');
              	return redirect('dashboard')->with(compact('sid','sname'));  


        }
        else{

              	$log_error=$request->session()->put('log_error','username or password is incorrect!!!');

            return view('layouts/login')->with(compact('log_error'));



              }
        } 
}
