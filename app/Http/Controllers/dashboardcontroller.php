<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\dashboardmodel;
use Illuminate\Support\Facades\Redirect;

class dashboardcontroller extends Controller
{
	public function __construct(){

		$user_id=\Illuminate\Support\Facades\Session::get('user_id');
		if($user_id== null){
			Redirect::to('/')->send();
		}
	}

	public function index(){
		return view('layouts/dashboard ');
	}
    public function store(Request $request){
    	$org_name=$_POST['org_name'];
    	$email=$_POST['email'];
    	$org_type=$_POST['org_type'];
    	$contact_no=$_POST['contact_no'];
    	$org_address=$_POST['org_address'];
    	$password=md5($_POST['password']);

    	$dashboardmodel = new dashboardmodel();

    	$user= $dashboardmodel->user_info_store($org_name,$email,$org_type,$contact_no,$org_address,$password);
        if($user == null){
        	$msg=$request->session()->put('error','registraton fail!!!');
        	return redirect ('create_user_account');
        }
        else{
        	$msg=$request->session()->put('success','registraton successfull!!!');
        	return redirect ('create_user_account')->with(compact('msg'));
        }

    }
    public function log_out(Request $request){
        $request->session()->forget('user_id');
        $request->session()->forget('name');
        $request->session()->forget('log_error');
        return redirect('/');
    }
}
