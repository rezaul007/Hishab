<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\invoiceModel;
use App\profileModel;
use PDF;
use App;
use Illuminate\Support\Facades\Redirect;

class invoiceController extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request){
        $invoice = null;
        $v=null;
        $vl= null;
        $vll= null;

        return view('layouts/invoice')->with(compact('invoice','vl','vll','v')) ;
    }
    public function client_view(Request $request){
        $v=null;
        $vl= null;
        $vll= null;
        $vl=$_POST['client'];
        $vll=$_POST['supplier'];
        $user_id=$request->session()->get('user_id');
        if($_POST['client'] != null && $_POST['supplier']==null){
            $v=$_POST['client'];
        }
        elseif($_POST['supplier'] != null && $_POST['client']==null) {
            $v=$_POST['supplier'];
        }
      //var_dump($v,$vl);
        //die();
        $model = new invoiceModel();
        $invoice = $model->view($user_id,$v);
        return view('layouts/invoice')->with(compact('invoice','v','vl','vll'));
    }

    public function pdfview(Request $request, $selected,$amount)
    {

        $user_id=$request->session()->get('user_id');
        $model = new invoiceModel();
        $invoice = $model->view($user_id,$selected);

        $model = new profileModel();
        $value=$model->view($user_id);
        foreach ($value as $v){
            $logo=$v->image;

        }
       // echo'<img  src="{{ URL::asset(\'images/\') }}" class="img-circle" alt="Company Logo">';
            //die;
            foreach ($invoice as $v){
        $data = <<<HTML
         <img height="50" width="50" src="images/$logo" class="img-circle" alt="Company Logo">
         <br><br>
        $v->pay_to
        <br>
        
        $v->clisub_address
        <br>
        <br>
        <br>
HTML;
    }
        $data .=<<<HTML
          <table border="1" width="100%">
                        <tr>
                            <th >Date</th>
                            <th >Item Description</th>
                            <th>Amount</th>
                        </tr>
                        
                        
HTML;
        $totalAmount = 0;
        foreach ($invoice as $value) {

            $amount=$value->amount;
            $totalAmount +=$amount;
            $data .= <<<HTML
           <tr>
               <td >$value->date</td>
               <td >$value->description</td>
               <td >$value->amount</td>
            
               
HTML;

        }
        $data .= <<<HTML
         </tr>
         <tr>
         <td colspan="2">Total</td>
         <td > $totalAmount</td>
         </tr>
         
         <h3><b>Note:All amount is in TK.</b></h3>
HTML;


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        return $pdf->stream();


    }
}
