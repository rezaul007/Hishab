<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\clientmodel;
use Illuminate\Support\Facades\Redirect;

class clientcontroller extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(Request $request){
    $user_id=$request->session()->get('user_id');  
    $model= new clientmodel();
    $client=$model->view($user_id);
    /*foreach ($client as $value) {
    		$name=$value->name;
    		$id=$value->id;
    		$org_name=$value->org_name; 
    		$category=$value->category;
    	}*/
   	return view('layouts/client_view')->with(compact('client'));
    }
   public function create(){
   	return view('layouts/client_add');
   }
   public function store(Request $request){
       $this->validate($request, [
           'name' => 'required',
           'org_name' => 'required',
           'category' => 'required',
           'password' => 'required',
       ]);
        $user_id=$request->session()->get('user_id');
    	$name = $_POST['name'];
    	$org_name = $_POST['org_name'];
    	$category = $_POST['category'];
    	$password = $_POST['password'];

    	$model= new clientmodel();
    	$client=$model->add_client($user_id,$name,$org_name,$category,$password);
    	if($client == true){
         $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('client_view')->with(compact('msg'));
    		
    	}
      else{
        $request->session()->put('msg','store failed');
        $msg=$request->session()->get('msg');
        return redirect('client_view')->with(compact('msg')); 
      }
   }
    public function edit(Request $request, $id){
       $model= new clientmodel();
       $client=$model->edit($id);
       foreach ($client as $value) {
            $id=$value->id;
            $name=$value->name;
            $org_name=$value->org_name;
            $category= $value->category;
            $password= $value->password;
       }
       return view('layouts/client_edit')->with(compact('id','name','org_name','category','password'));


     }
     
      public function update_client(Request $request){
      $id=$_POST['id'];
      $name = $_POST['name'];
      $org_name = $_POST['org_name'];
      $category = $_POST['category'];
      $model= new clientmodel();
      $client=$model->update_client($id,$name,$org_name,$category);
      if($client == true){
         $request->session()->put('msg','update successfull');
        $msg=$request->session()->get('msg');
        return redirect('client_view')->with(compact('msg'));
      
      }
      else{
           $request->session()->put('msg','update successfull');
           $msg=$request->session()->get('msg');
          return redirect('client_view')->with(compact('msg'));
      }
    }
    public function delete_client(Request $request,$id){
    	 $model= new clientmodel();
    	$delete=$model->client_delete($id);
    	if($delete != null){
        $request->session()->put('msg','delete successfull');
        $msg=$request->session()->get('msg');
        return redirect('client_view')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','delete failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('client_view')->with(compact('msg'));
      }
    }

}
