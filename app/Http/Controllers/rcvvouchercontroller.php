<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\rcvvouchermodel;
use Illuminate\Support\Facades\Redirect;


class rcvvouchercontroller extends Controller
{
    public function __construct(){

        $user_id=\Illuminate\Support\Facades\Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function index(){
    	return view('layouts/receipt_voucher');
    }
    public function store(Request $request){

        $this->validate($request, [
            'date' => 'required',
            'description' => 'required',
            'ac_head' => 'required',
            'amount' => 'required',
            'rcv_from' => 'required',

        ]);
      $user_id=$request->session()->get('user_id');
      $date=$_POST['date'];
      $description=$_POST['description'];
      $ac_head=$_POST['ac_head'];
      $amount=$_POST['amount'];
      $pay_to=$_POST['rcv_from'];
      $voucher_type="receipt";
        $address = $_POST['address'];

      $model=new rcvvouchermodel();
      $voucher=$model->add_voucher($date,$description,$ac_head,$amount,$pay_to,$voucher_type,$user_id,$address);
      if($voucher != null){
        $request->session()->put('msg','store successfull');
        $msg=$request->session()->get('msg');
        return redirect('view_voucher')->with(compact('msg'));
    	}
      else{
        $request->session()->put('msg','store failed!!!');
        $msg=$request->session()->get('msg');
        return redirect('view_voucher')->with(compact('msg'));
      }

    }
}
