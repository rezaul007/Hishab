<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbmodel;
use Excel;
use Illuminate\Support\Facades\Redirect;

class excelController extends Controller
{
    public function __construct(){

        $user_id=Session::get('user_id');
        if($user_id== null){
            Redirect::to('/')->send();
        }
    }
    public function downloadExcel(Request $request, $type)
    {
       /* $data = "hoisa";
        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);*/
        // Initialize the array which will be passed into the Excel
        // generator.
        $user_id=$request->session()->get('user_id');
        $model=new tbmodel();
        $sn=$model->sn();
        $voucher=$model->view($user_id);
        $paymentsArray = [];

        // Define the Excel spreadsheet headers
        $paymentsArray[] = ['S/N', 'Head of the account ','Debit amount','Credit ','total'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($voucher as $payment) {
            $paymentsArray[] = $payment->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('payments', function($excel) use ($paymentsArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('trialbalance');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('trialbalance sheet');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($paymentsArray) {
                $sheet->fromArray($paymentsArray, null, 'A1', false, false);
            });

        })->download('xlsx');
    }
}
