<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class ac_ledgermodel extends Model
{
    public function view($user_id){
        $user=DB::table('account_ledger')
        ->select(DB::raw('*'))
        ->where('user_id','=',$user_id)
        ->get();
        return $user;
    }
    public function add_ac_ledger($ledger_name,$ledger_group,$tranaction_type,$date,$user_id){
    	$user=DB::table('account_ledger')
    	->insert( ['ledger_name'=>$ledger_name,'ledger_group_id'=>$ledger_group,'user_id'=>$user_id,'transaction_type'=>$tranaction_type,'date'=>$date] );
 
    	return $user;

    }
     public function ac_lg_delete($id){
        $user=DB::table('account_ledger')
        ->where('id','=',$id)
    	->delete();
    	return $user;
    }
}
