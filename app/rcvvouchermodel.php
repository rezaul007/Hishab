<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class rcvvouchermodel extends Model
{
    public function add_voucher($date,$description,$ac_head,$amount,$pay_to,$voucher_type,$user_id,$address){
    	$user=DB::table('voucher')
    	->insert( ['date'=>$date,'description'=>$description,'accounting_ledger'=>$ac_head,'amount'=>$amount,'pay_to'=>$pay_to,'voucher_type'=>$voucher_type,'user_id'=>$user_id,'clisub_address'=>$address] );
 
    	return $user;

    }
    public function view($user_id){
        $user=DB::table('voucher')
        ->join('account_ledger', 'account_ledger.id', '=', 'voucher.accounting_ledger')
        ->select('voucher.*', 'account_ledger.ledger_name')
        ->where('voucher.user_id','=',$user_id)
        ->get();
        return $user;
        
    }
    public function curr_amount($ac_head,$user_id){
    	$amount=DB::table('voucher')
    	->select(DB::raw(sum('amount')))
    	->where('ac_head','=',$ac_head)
        ->where('user_id','=',$user_id)
    	->get();
    	return $amount;
    }
}
