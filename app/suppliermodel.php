<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class suppliermodel extends Model
{
    public function add_supplier($user_id,$name,$org_name,$category,$password){
    	$user=DB::table('supplier')
    	->insert( ['name'=>$name,'org_name'=>$org_name,'category'=>$category,'password'=>$password,'user_id'=>$user_id] );
    	
    	return true;

    }
    public function view($user_id){
        $user=DB::table('supplier')
        ->select(DB::raw('*'))
        ->where('user_id','=',$user_id)
        ->get();
        return $user;
        
    }
     public function edit($id){
        $user=DB::table('supplier')
        ->select(DB::raw('*'))
        ->where('id','=',$id)
        ->get();
        return $user;
        
    }
    public function update_supplier($id,$name,$org_name,$category){
    	$supplier=DB::table('supplier')
    	          ->where('id','=',$id)
    	         ->update(['name'=>$name,'org_name'=>$org_name,'category'=>$category]);
    	        
    	         return true;
    }
    public function supplier_delete($id){
        $user=DB::table('supplier')
        ->where('id','=',$id)
    	->delete();
    	return $user;
    }
}
