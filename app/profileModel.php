<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class profileModel extends Model
{
    public function view($user_id){
        $user=DB::table('user')
            ->select(DB::raw('*'))
            ->where('user_id','=',$user_id)
            ->get();
        return $user;
    }
    public function update_profile($org_name,$email,$contact_no,$address,$org_type,$id){

        $supplier=DB::table('user')
            ->where('user_id','=',$id)
            ->update(['email'=>$email,'org_name'=>$org_name,'contact_no'=>$contact_no,'org_address'=>$address,'org_type'=>$org_type]);

        return $supplier;
    }
    public function view2($user_id){
        $user=DB::table('user')
            ->select(DB::raw('*'))
            ->where('user_id','=',$user_id)
            ->get();
        return $user;

    }
    public function confpass($user_id){
        $user=DB::table('user')
            ->select(DB::raw('*'))
            ->where('user_id','=',$user_id)
            ->get();
        return $user;

    }
    public function new_pass($pass){
        $user=DB::table('user')
            ->update(['password'=>$pass]);

        return $user;

    }
    public function img_up($img,$user_id){
        $query=DB::select("UPDATE user SET user.image='$img' WHERE user.user_id=$user_id");
        //return $query;
//
        return $query;
    }

}
