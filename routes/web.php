<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$log_error=session()->get('log_error');
    if($log_error==null){
    $log_error=null;
    }
    else{
        $log_error=session()->get('log_error');
    }
    return view('layouts/login')->with(compact('log_error'));
});
Route::get('create_user_account','usercontroller@index');
Route::post('dashboard_store','dashboardcontroller@store');
Route::get('dashboard','dashboardcontroller@index');
Route::post('user_login','usercontroller@login');
Route::get('supplier_add','suppliercontroller@create');
Route::get('supplier_view','suppliercontroller@index');
Route::post('supplier_store','suppliercontroller@store');
Route::get('supplier_edit/{id}','suppliercontroller@edit');
Route::post('supplier_update','suppliercontroller@update_supplier');
Route::get('add_client','clientcontroller@create');
Route::post('client_store','clientcontroller@store');
Route::get('client_view','clientcontroller@index');
Route::get('client_edit/{id}','clientcontroller@edit');
Route::post('client_update','clientcontroller@update_client');
Route::get('log_out','dashboardcontroller@log_out');
Route::get('add_pay_voucher','payvoucontroller@index');
Route::get('add_rcv_voucher','rcvvouchercontroller@index');
Route::post('store_pay_voucher','payvoucontroller@store');
Route::get('view_voucher','payvoucontroller@view');
Route::post('store_rcv_voucher','rcvvouchercontroller@store');
Route::get('view_ledger_group','ledger_group_controller@index');
Route::post('add_ledger_group','ledger_group_controller@store');
Route::get('delete_ledger_group/{id}','ledger_group_controller@delete_ledger_group');
Route::get('ac_ledger_view','ac_ledgercontroller@index');
Route::post('ac_ledger_store','ac_ledgercontroller@store');
Route::get('ac_ledger_delete/{id}','ac_ledgercontroller@ac_ledger_delete');

Route::post('caa','payvoucontroller@curramount');

Route::get('supplier_delete/{id}','suppliercontroller@delete_supplir');
Route::get('delete_client/{id}','clientcontroller@delete_client');

Route::get('trial_balence','trialbalenceController@index');
Route::get('profile_view','profileController@view');
Route::get('profile_edit','profileController@edit');
Route::post('profile_update','profileController@update_profile');
Route::post('pass_update','profileController@resetPass');
Route::get('pass_from','profileController@view_pass_from');
Route::get('view_invoice','invoiceController@index');
Route::post('view_client','invoiceController@client_view');
Route::get('pdfview/{selected?}/{amount?}','invoiceController@pdfview');
Route::get('img_from','profileController@img_from');
Route::post('img_store','profileController@img_upload');
Route::get('downloadExcel/{type}','excelController@downloadExcel');


